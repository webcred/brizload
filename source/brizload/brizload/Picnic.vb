﻿'Copyright (c) 2012 Steve Shearn. All rights reserved.

'Permission is hereby granted, free of charge, to any person obtaining a
'copy of this software and associated documentation files (the "Software"), 
'to deal in the Software without restriction, including without limitation 
'the rights to use, copy, modify, merge, publish, distribute, sublicense, 
'and/or sell copies of the Software, and to permit persons to whom the 
'Software is furnished to do so, subject to the following conditions:

'The above copyright notice and this permission notice shall be included in
'all copies or substantial portions of the Software.

'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
'FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
'FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
'DEALINGS IN THE SOFTWARE.

Public Class Picnic

    Private append As Boolean = False

    Public Sub New(ByVal append As Boolean)
        Me.append = append
    End Sub
    ' id,Park Code,Park Name,Street,Suburb,Picnic Area Node Id,Picnic Area Name
    Public Function CheckHeader(ByVal cols() As String) As Boolean
        If cols.Count <> 7 Then Return False

        If Not cols(0).Trim.Equals("ID", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(1).Trim.Equals("Park Code", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(2).Trim.Equals("Park Name", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(3).Trim.Equals("Street", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(4).Trim.Equals("Suburb", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(5).Trim.Equals("Picnic Area Node Id", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(6).Trim.Equals("Picnic Area Name", StringComparison.CurrentCultureIgnoreCase) Then Return False

        Return True
    End Function

    Public Sub Parse(ByVal Conn As Data.OleDb.OleDbConnection, ByVal outpath As String)
        Dim sql = "SELECT * FROM PICNIC_AREAS"
        Using Cmd As New OleDb.OleDbCommand(sql, Conn)
            Dim rdr = Cmd.ExecuteReader()
            Using sw As New System.IO.StreamWriter(outpath, Me.append)
                While rdr.Read()
                    Dim sbName As New System.Text.StringBuilder()
                    sbName.Append(rdr(6))
                    Module1.CleanUp(sbName)

                    Dim sbAddress As New System.Text.StringBuilder()
                    sbAddress.Append(rdr(2).ToString)
                    sbAddress.Append(vbCrLf & rdr(3).ToString)
                    sbAddress.Append("," & vbCrLf & rdr(4).ToString)
                    Module1.CleanUp(sbAddress)

                    Dim sbInfo As New System.Text.StringBuilder()
                    Module1.CleanUp(sbInfo)

                    Dim latlng() As String
                    Try
                        latlng = Geo.GetGeoCode(rdr(3).ToString & ", " & rdr(4).ToString & " QLD Australia")
                        If latlng.Count <> 2 Then
                            ' try again
                            latlng = Geo.GetGeoCode(rdr(3).ToString & ", " & rdr(4).ToString & " QLD Australia")
                        End If
                        If latlng.Count <> 2 Then Continue While
                    Catch ex As Exception
                        Console.WriteLine("Skipped")
                        Continue While
                    End Try

                    Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                    sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                    sb.Append(" VALUES (")
                    sb.Append("""picnic"",")
                    sb.Append("""" & latlng(0).ToString & """,")
                    sb.Append("""" & latlng(1).ToString & """,")
                    sb.Append("""" & sbName.ToString & """,")
                    sb.Append("""" & sbAddress.ToString & """,")
                    sb.Append("""" & sbInfo.ToString & """")
                    sb.Append(");")

                    sw.WriteLine(sb.ToString)
                End While

                sw.Close()
            End Using
        End Using
    End Sub

    Public Sub Parse(ByVal rdr As Microsoft.VisualBasic.FileIO.TextFieldParser, ByVal outpath As String)
        Using sw As New System.IO.StreamWriter(outpath, Me.append)
            While Not rdr.EndOfData
                Dim vals = rdr.ReadFields()

                Dim sbName As New System.Text.StringBuilder()
                sbName.Append(vals(6))
                Module1.CleanUp(sbName)

                Dim sbAddress As New System.Text.StringBuilder()
                sbAddress.Append(vals(2))
                sbAddress.Append(vbCrLf & vals(3))
                sbAddress.Append("," & vbCrLf & vals(4))
                Module1.CleanUp(sbAddress)

                Dim sbInfo As New System.Text.StringBuilder()
                Module1.CleanUp(sbInfo)

                Dim latlng() As String
                Try
                    latlng = Geo.GetGeoCode(vals(3).ToString & ", " & vals(4).ToString & " QLD Australia")
                    If latlng.Count <> 2 Then
                        ' try again
                        latlng = Geo.GetGeoCode(vals(3).ToString & ", " & vals(4).ToString & " QLD Australia")
                    End If
                    If latlng.Count <> 2 Then Continue While
                Catch ex As Exception
                    Console.WriteLine("Skipped")
                    Continue While
                End Try

                Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                sb.Append(" VALUES (")
                sb.Append("""picnic"",")
                sb.Append("""" & latlng(0).ToString & """,")
                sb.Append("""" & latlng(1).ToString & """,")
                sb.Append("""" & sbName.ToString & """,")
                sb.Append("""" & sbAddress.ToString & """,")
                sb.Append("""" & sbInfo.ToString & """")
                sb.Append(");")

                sw.WriteLine(sb.ToString)
            End While

            sw.Close()
        End Using
    End Sub

End Class
