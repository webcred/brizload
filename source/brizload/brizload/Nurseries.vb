﻿'Copyright (c) 2012 Steve Shearn. All rights reserved.

'Permission is hereby granted, free of charge, to any person obtaining a
'copy of this software and associated documentation files (the "Software"), 
'to deal in the Software without restriction, including without limitation 
'the rights to use, copy, modify, merge, publish, distribute, sublicense, 
'and/or sell copies of the Software, and to permit persons to whom the 
'Software is furnished to do so, subject to the following conditions:

'The above copyright notice and this permission notice shall be included in
'all copies or substantial portions of the Software.

'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
'FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
'FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
'DEALINGS IN THE SOFTWARE.

Public Class Nurseries

    Private append As Boolean = False

    Public Sub New(ByVal append As Boolean)
        Me.append = append
    End Sub

    Public Function CheckHeader(ByVal cols() As String) As Boolean
        If cols.Count <> 7 Then Return False

        If Not cols(0).Trim.Equals("ID", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(1).Trim.Equals("Nursery Name", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(2).Trim.Equals("Address", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(3).Trim.Equals("Suburb", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(4).Trim.Equals("Phone Number", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(5).Trim.Equals("Latitude", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(6).Trim.Equals("Longitude", StringComparison.CurrentCultureIgnoreCase) Then Return False

        Return True
    End Function

    Public Sub Parse(ByVal rdr As Microsoft.VisualBasic.FileIO.TextFieldParser, ByVal outpath As String)
        Using sw As New System.IO.StreamWriter(outpath, Me.append)
            While Not rdr.EndOfData
                Dim vals = rdr.ReadFields()

                If String.IsNullOrEmpty(vals(5)) Then Continue While
                If String.IsNullOrEmpty(vals(6)) Then Continue While

                Dim sbName As New System.Text.StringBuilder()
                sbName.Append(vals(1))
                Module1.CleanUp(sbName)

                Dim sbAddress As New System.Text.StringBuilder()
                sbAddress.Append(vals(2))
                sbAddress.Append("," & vbCrLf & vals(3))
                Module1.CleanUp(sbAddress)

                Dim sbInfo As New System.Text.StringBuilder()
                sbInfo.Append("Ph: " & vals(4))
                Module1.CleanUp(sbInfo)

                Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                sb.Append(" VALUES (")
                sb.Append("""nursery"",")
                sb.Append("""" & vals(5) & """,")
                sb.Append("""" & vals(6) & """,")
                sb.Append("""" & sbName.ToString & """,")
                sb.Append("""" & sbAddress.ToString & """,")
                sb.Append("""" & sbInfo.ToString & """")
                sb.Append(");")

                sw.WriteLine(sb.ToString)
            End While

            sw.Close()
        End Using
    End Sub

End Class
