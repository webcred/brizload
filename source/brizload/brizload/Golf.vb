﻿'Copyright (c) 2012 Steve Shearn. All rights reserved.

'Permission is hereby granted, free of charge, to any person obtaining a
'copy of this software and associated documentation files (the "Software"), 
'to deal in the Software without restriction, including without limitation 
'the rights to use, copy, modify, merge, publish, distribute, sublicense, 
'and/or sell copies of the Software, and to permit persons to whom the 
'Software is furnished to do so, subject to the following conditions:

'The above copyright notice and this permission notice shall be included in
'all copies or substantial portions of the Software.

'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
'FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
'FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
'DEALINGS IN THE SOFTWARE.

Public Class Golf

    Private append As Boolean = False

    Public Sub New(ByVal append As Boolean)
        Me.append = append
    End Sub

    Public Function CheckHeader(ByVal cols() As String) As Boolean
        If cols.Count <> 9 Then Return False

        If Not cols(0).Trim.Equals("ID", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(1).Trim.Equals("Name", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(2).Trim.Equals("Address", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(3).Trim.Equals("Phone No", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(4).Trim.Equals("Opening Hours", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(5).Trim.Equals("Facilities", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(6).Trim.Equals("Parking", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(7).Trim.Equals("Latitude", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(8).Trim.Equals("Longitude", StringComparison.CurrentCultureIgnoreCase) Then Return False

        Return True
    End Function

    Public Sub Parse(ByVal Conn As Data.OleDb.OleDbConnection, ByVal outpath As String)
        Dim sql = "SELECT * FROM GOLF_COURSES"
        Using Cmd As New OleDb.OleDbCommand(sql, Conn)
            Dim rdr = Cmd.ExecuteReader()
            Using sw As New System.IO.StreamWriter(outpath, Me.append)
                While rdr.Read()
                    If String.IsNullOrEmpty(rdr(7).ToString) Then Continue While
                    If String.IsNullOrEmpty(rdr(8).ToString) Then Continue While

                    Dim sbName As New System.Text.StringBuilder()
                    sbName.Append(rdr(1))
                    Module1.CleanUp(sbName)

                    Dim sbAddress As New System.Text.StringBuilder()
                    sbAddress.Append(rdr(2).ToString)
                    Module1.CleanUp(sbAddress)

                    Dim sbInfo As New System.Text.StringBuilder()
                    sbInfo.Append("Ph: " & rdr(3).ToString)
                    sbInfo.Append(vbCrLf & "Opening Hours: " & rdr(4).ToString)
                    sbInfo.Append(vbCrLf & "Facilities: " & rdr(5).ToString)
                    sbInfo.Append(vbCrLf & "Parking: " & rdr(6).ToString)
                    Module1.CleanUp(sbInfo)

                    Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                    sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                    sb.Append(" VALUES (")
                    sb.Append("""golf"",")
                    sb.Append("""" & rdr(7).ToString & """,")
                    sb.Append("""" & rdr(8).ToString & """,")
                    sb.Append("""" & sbName.ToString & """,")
                    sb.Append("""" & sbAddress.ToString & """,")
                    sb.Append("""" & sbInfo.ToString & """")
                    sb.Append(");")

                    sw.WriteLine(sb.ToString)
                End While

                sw.Close()
            End Using
        End Using
    End Sub

    Public Sub Parse(ByVal rdr As Microsoft.VisualBasic.FileIO.TextFieldParser, ByVal outpath As String)
        Using sw As New System.IO.StreamWriter(outpath, Me.append)
            While Not rdr.EndOfData
                Dim vals = rdr.ReadFields()

                If String.IsNullOrEmpty(vals(7)) Then Continue While
                If String.IsNullOrEmpty(vals(8)) Then Continue While

                Dim sbName As New System.Text.StringBuilder()
                sbName.Append(vals(1))
                Module1.CleanUp(sbName)

                Dim sbAddress As New System.Text.StringBuilder()
                sbAddress.Append(vals(2))
                Module1.CleanUp(sbAddress)

                Dim sbInfo As New System.Text.StringBuilder()
                sbInfo.Append("Ph: " & vals(3))
                sbInfo.Append(vbCrLf & "Opening Hours: " & vals(4))
                sbInfo.Append(vbCrLf & "Facilities: " & vals(5))
                sbInfo.Append(vbCrLf & "Parking: " & vals(6))
                Module1.CleanUp(sbInfo)

                Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                sb.Append(" VALUES (")
                sb.Append("""golf"",")
                sb.Append("""" & vals(7) & """,")
                sb.Append("""" & vals(8) & """,")
                sb.Append("""" & sbName.ToString & """,")
                sb.Append("""" & sbAddress.ToString & """,")
                sb.Append("""" & sbInfo.ToString & """")
                sb.Append(");")

                sw.WriteLine(sb.ToString)
            End While

            sw.Close()
        End Using
    End Sub

End Class
