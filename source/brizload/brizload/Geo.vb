﻿Public Class Geo

    Public Shared Function GetGeoCode(ByVal address As String) As String()
        Dim geocoder_url = "http://maps.google.com/maps/api/geocode/xml?address=" & address & "&sensor=false"

        Console.Write("    geocoding: '" & address & "'")

        Dim ret As New List(Of String)

        Dim webRequest = Net.WebRequest.Create(geocoder_url)
        Using webresponse = webRequest.GetResponse()
            Using inStream = New IO.StreamReader(webresponse.GetResponseStream())
                Dim html = inStream.ReadToEnd()

                ' just locate in string, don't bother parsing xml properly
                html = html.Substring(html.IndexOf("<location>"))

                ' get lat element
                Dim i = html.IndexOf("<lat>")
                If i < 0 Then
                    ret.Clear()
                    Console.WriteLine("  SKIPPED")
                    Return ret.ToArray
                End If
                i += 5
                Dim j = html.IndexOf("</lat>")
                ret.Add(html.Substring(i, j - i))

                i = html.IndexOf("<lng>")
                If i < 0 Then
                    ret.Clear()
                    Console.WriteLine("  SKIPPED")
                    Return ret.ToArray()
                End If
                i += 5
                j = html.IndexOf("</lng>")
                ret.Add(html.Substring(i, j - i))
            End Using
        End Using

        Console.WriteLine()

        Return ret.ToArray
    End Function

End Class
