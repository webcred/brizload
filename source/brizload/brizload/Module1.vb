﻿'Copyright (c) 2012 Steve Shearn. All rights reserved.

'Permission is hereby granted, free of charge, to any person obtaining a
'copy of this software and associated documentation files (the "Software"), 
'to deal in the Software without restriction, including without limitation 
'the rights to use, copy, modify, merge, publish, distribute, sublicense, 
'and/or sell copies of the Software, and to permit persons to whom the 
'Software is furnished to do so, subject to the following conditions:

'The above copyright notice and this permission notice shall be included in
'all copies or substantial portions of the Software.

'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
'FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
'FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
'DEALINGS IN THE SOFTWARE.

Module Module1

    'Private Const DEBUG_IN As String = "c:\temp\bcc.mdb"
    'Private Const DEBUG_OUT As String = "c:\temp\out.sql"
    Private Const DEBUG_IN As String = ""
    Private Const DEBUG_OUT As String = "out.sql"

    Sub Main()
        Console.WriteLine("Outside Brisbane dataset converter")

        Dim path = DEBUG_IN
        Dim out = DEBUG_OUT

        If String.IsNullOrEmpty(path) And String.IsNullOrEmpty(out) Then
            ' check usage
            If My.Application.CommandLineArgs.Count = 1 Then
                ' get only out
                path = ""
                out = My.Application.CommandLineArgs(0)
            ElseIf My.Application.CommandLineArgs.Count = 2 Then
                ' get paths
                path = My.Application.CommandLineArgs(0)
                out = My.Application.CommandLineArgs(1)
            Else
                Console.WriteLine("Useage: brizload <<source filename>> <<mysqlqueryout filename>>")
                Console.WriteLine(" (or)")
                Console.WriteLine("        brizload <<mysqlqueryout filename>>   [ this will loop through all csv files to the output ]")
                End
            End If
        End If

        Try
            If path.Length = 0 Then
                Dim here = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.Location())
                For Each file In New System.IO.DirectoryInfo(here).GetFiles("*.mdb")
                    Call parse(file.FullName, out)
                Next
                For Each file In New System.IO.DirectoryInfo(here).GetFiles("*.csv")
                    parse(file.FullName, out, True)
                Next
                Dim t As New Trains()
                For Each file In New System.IO.DirectoryInfo(here).GetFiles("*.aspx")
                    Console.WriteLine("qr website file: " & file.FullName)
                    t.parse(file.FullName)
                Next
                t.output(out)
            Else
                If System.IO.Path.GetExtension(path).Equals(".csv") Then
                    Call parse(path, out, False)
                ElseIf System.IO.Path.GetExtension(path).Equals(".mdb") Then
                    Call parse(path, out)
                ElseIf System.IO.Path.GetExtension(path).Equals(".aspx") Then
                    Console.WriteLine(path & ": qr website file")
                    Dim t As New Trains()
                    t.parse(path)
                    t.output(out)
                End If
            End If

        Catch ex As Exception
            Console.WriteLine("ERROR: " & ex.Message)

        End Try
    End Sub

    ' the .csv files are dodgy, will have to use the access database
    Private Sub parse(ByVal path As String, ByVal out As String)
        Dim connstr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & path & ";Persist Security Info=True;"
        Using Conn As New Data.OleDb.OleDbConnection(connstr)
            Conn.Open() ' gotta build as 32 bit app for this: http://answers.microsoft.com/en-us/office/forum/office_2010-access/the-microsoft-aceoledb120-provider-is-not/9a6b92fd-e19b-44c8-bea7-1e84679cdd5e

            Dim tennis As New Tennis(True)
            Dim park As New Parks(True)
            Dim skateboard As New Skateboard(True)
            Dim commgarden As New CommGarden(True)
            Dim pools As New Pools(True)
            Dim golf As New Golf(True)
            Dim pg As New Playground(True)
            Dim pt As New Toilets(True)
            Dim dp As New DogParks(True)
            Dim pa As New PublicArt(True)
            Dim bike As New CityCycle(True)
            Dim wifi As New Wifi(True)
            Dim bus As New BusStops(True)
            Dim boat As New Boat(True)
            Dim ferry As New Ferry(True)
            Dim picnic As New Picnic(True)
            Dim ratings As New AccessRating(True)

            Try
                Console.WriteLine(path & ": Tennis courts dataset")
                tennis.Parse(Conn, out)

                Console.WriteLine(path & ": Parks dataset")
                park.Parse(Conn, out)

                Console.WriteLine(path & ": Skateboard parks")
                skateboard.Parse(Conn, out)

                Console.WriteLine(path & ": Communuity gardens")
                commgarden.Parse(Conn, out)

                Console.WriteLine(path & ": Swimming pools")
                pools.Parse(Conn, out)

                Console.WriteLine(path & ": Golf courses")
                golf.Parse(Conn, out)

                Console.WriteLine(path & ": Playgrounds")
                pg.Parse(Conn, out)

                Console.WriteLine(path & ": Public toilets")
                pt.Parse(Conn, out)

                Console.WriteLine(path & ": Dog parks")
                dp.Parse(Conn, out)

                Console.WriteLine(path & ": Public art")
                pa.Parse(Conn, out)

                Console.WriteLine(path & ": City cycle stations")
                bike.Parse(Conn, out)

                Console.WriteLine(path & ": Wifi hotspots")
                wifi.Parse(Conn, out)

                Console.WriteLine(path & ": Bus stops")
                bus.Parse(Conn, out)

                Console.WriteLine(path & ": Boat ramps")
                boat.Parse(Conn, out)

                Console.WriteLine(path & ": Ferry terminals")
                ferry.Parse(Conn, out)




                Console.WriteLine(path & ": Picnic areas")
                picnic.Parse(Conn, out)


                Console.WriteLine(path & ": Access ratings")
                ratings.Parse(Conn, out)


            Catch ex As Exception
                Console.WriteLine("Failed: " & ex.Message)

            End Try

            Conn.Close()
        End Using
    End Sub

    Private Sub parse(ByVal path As String, ByVal out As String, ByVal append As Boolean)
        If System.IO.Path.GetExtension(path).Equals(".csv", StringComparison.CurrentCultureIgnoreCase) Then
            ' CSV file
            Using rdr As New Microsoft.VisualBasic.FileIO.TextFieldParser(path)
                rdr.TextFieldType = FileIO.FieldType.Delimited
                rdr.SetDelimiters(",")

                ' check row for how to parse
                If Not rdr.EndOfData Then
                    Dim row = rdr.ReadFields()

                    Dim tennis As New Tennis(append)
                    Dim park As New Parks(append)
                    Dim skateboard As New Skateboard(append)
                    Dim commgarden As New CommGarden(append)
                    Dim pools As New Pools(append)
                    Dim golf As New Golf(append)
                    Dim pg As New Playground(append)
                    Dim pt As New Toilets(append)
                    Dim dp As New DogParks(append)
                    Dim pa As New PublicArt(append)
                    Dim bike As New CityCycle(append)
                    Dim wifi As New Wifi(append)
                    Dim nurseries As New Nurseries(append)
                    Dim pets As New Pets(append)
                    Dim helmets As New Helmets(append)
                    Dim bus As New BusStops(append)
                    Dim boat As New Boat(True)
                    Dim ferry As New Ferry(True)
                    Dim picnic As New Picnic(append)
                    Dim ratings As New AccessRating(append)

                    If tennis.CheckHeader(row) Then
                        Console.WriteLine(path & ": Tennis courts dataset")
                        tennis.Parse(rdr, out)
                    ElseIf park.CheckHeader(row) Then
                        Console.WriteLine(path & ": Parks dataset")
                        park.Parse(rdr, out)
                    ElseIf skateboard.CheckHeader(row) Then
                        Console.WriteLine(path & ": Skateboard parks")
                        skateboard.Parse(rdr, out)
                    ElseIf commgarden.CheckHeader(row) Then
                        Console.WriteLine(path & ": Communuity gardens")
                        commgarden.Parse(rdr, out)
                    ElseIf pools.CheckHeader(row) Then
                        Console.WriteLine(path & ": Swimming pools")
                        pools.Parse(rdr, out)
                    ElseIf golf.CheckHeader(row) Then
                        Console.WriteLine(path & ": Golf courses")
                        golf.Parse(rdr, out)
                    ElseIf pg.CheckHeader(row) Then
                        Console.WriteLine(path & ": Playgrounds")
                        pg.Parse(rdr, out)
                    ElseIf pt.CheckHeader(row) Then
                        Console.WriteLine(path & ": Public toilets")
                        pt.Parse(rdr, out)
                    ElseIf dp.CheckHeader(row) Then
                        Console.WriteLine(path & ": Dog parks")
                        dp.Parse(rdr, out)
                    ElseIf pa.CheckHeader(row) Then
                        Console.WriteLine(path & ": Public art")
                        pa.Parse(rdr, out)
                    ElseIf bike.CheckHeader(row) Then
                        Console.WriteLine(path & ": City cycle stations")
                        bike.Parse(rdr, out)
                    ElseIf wifi.CheckHeader(row) Then
                        Console.WriteLine(path & ": Wifi hotspots")
                        wifi.Parse(rdr, out)
                    ElseIf nurseries.CheckHeader(row) Then
                        Console.WriteLine(path & ": Nurseries")
                        nurseries.Parse(rdr, out)
                    ElseIf pets.CheckHeader(row) Then
                        Console.WriteLine(path & ": Pet shops")
                        pets.Parse(rdr, out)
                    ElseIf helmets.CheckHeader(row) Then
                        Console.WriteLine(path & ": Helmet retailers")
                        helmets.Parse(rdr, out)
                    ElseIf bus.CheckHeader(row) Then
                        Console.WriteLine(path & ": Bus stops")
                        bus.Parse(rdr, out)
                    ElseIf boat.CheckHeader(row) Then
                        Console.WriteLine(path & ": Boat ramps")
                        boat.Parse(rdr, out)
                    ElseIf ferry.CheckHeader(row) Then
                        Console.WriteLine(path & ": Ferry terminals")
                        ferry.Parse(rdr, out)


                    ElseIf picnic.CheckHeader(row) Then
                        Console.WriteLine(path & ": Picnic areas")
                        picnic.Parse(rdr, out)

                    ElseIf ratings.CheckHeader(row) Then
                        Console.WriteLine(path & ": Access ratings")
                        ratings.Parse(rdr, out)

                    Else
                        Console.WriteLine(path & ": Unknown dataset")
                    End If
                End If

                rdr.Close()
            End Using
        End If
    End Sub

    Public Sub CleanUp(ByRef sb As System.Text.StringBuilder)
        sb.Replace("""", """""")
        sb.Replace(vbCrLf, "<br />")
        sb.Replace(vbCr, "<br />")
        sb.Replace(vbLf, "<br />")
    End Sub

End Module
