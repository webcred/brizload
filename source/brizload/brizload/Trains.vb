﻿Public Class Trains

    Private Class station
        Public lat As String = ""
        Public lng As String = ""
        Public name As String = ""
    End Class

    ' there'll be duplicates
    Private Class CompareStationName
        Implements IEqualityComparer(Of station)

        Public Shadows Function Equals(ByVal x As station, ByVal y As station) As Boolean Implements System.Collections.Generic.IEqualityComparer(Of station).Equals
            If [Object].ReferenceEquals(x, y) Then
                Return True
            End If
            If x Is Nothing OrElse y Is Nothing Then
                Return False
            End If
            Return x.name = y.name
        End Function

        Public Shadows Function GetHashCode(ByVal obj As station) As Integer Implements System.Collections.Generic.IEqualityComparer(Of station).GetHashCode
            If obj Is Nothing Then
                Return 0
            End If
            Return obj.name.GetHashCode()
        End Function
    End Class

    Private stations As New List(Of station)

    Public Sub parse(ByVal path As String)
        ' load in file
        Dim sb As New System.Text.StringBuilder()
        Using file = System.IO.File.OpenText(path)
            Dim s = file.ReadLine()
            sb.AppendLine(s)
            While s IsNot Nothing
                s = file.ReadLine()
                sb.AppendLine(s)
            End While
        End Using

        ' look for latlng code. just look for this pattern:
        'var latlong0 = new GLatLng(-27.385044, 153.118978);
        'var mkr0 = new GMarker(latlong0, { icon:customIcon, title:'Airport Domestic'  } );googleMap.addOverlay(mkr0);

        ' find title first, the get the previous GLatLng declaration for its co-ordinate

        Dim source = sb.ToString
        Dim previdx = 0
        Dim curridx = 0
        curridx = source.IndexOf("title:'", curridx)
        While True
            Dim station As New station()

            ' get station name
            curridx = source.IndexOf("title:'", curridx)
            If curridx = -1 Then Exit While

            curridx += "title:'".Length
            station.name = source.Substring(curridx, source.IndexOf("'", curridx) - curridx)

            ' get geo declaration
            Dim latlngidx = source.IndexOf("new GLatLng(", previdx)
            Dim latlng = source.Substring(latlngidx + "new GLatLng(".Length, curridx)
            latlng = latlng.Substring(0, latlng.IndexOf(");"))

            station.lat = latlng.Substring(0, latlng.IndexOf(","))
            station.lng = latlng.Substring(latlng.IndexOf(",") + 1)

            Me.stations.Add(station)

            previdx = curridx
        End While
    End Sub

    Public Sub output(ByVal out As String)
        Using sw As New System.IO.StreamWriter(out, True)
            For Each station In Me.stations.Distinct(New CompareStationName())
                Dim sb As New System.Text.StringBuilder()
                sb.Append("INSERT INTO `places` (`code`, `latitude`, `longitude`, `name`, `address`, `info`) VALUES (""train"",")
                sb.Append("""" & station.lat & """,")
                sb.Append("""" & station.lng & """,")
                sb.Append("""" & station.name & """,")
                sb.Append("""" & """,")
                sb.Append("""" & """")
                sb.Append(");")
                sw.WriteLine(sb.ToString)
            Next
        End Using
    End Sub

End Class
