﻿044233.csv
Tennis courts updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/tennis-courts/

044224.csv
Parks updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/parks-2/

044229.csv
Skateboard parks updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/skateboard-parks/

044210.csv
Community gardens updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/community-gardens/

044232.csv
Swimming pools updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/swimming-pools/

044216.csv
Golf courses updated yearly (if at all) :)
http://data.brisbane.qld.gov.au/index.php/dataset/golf-courses/

044225.csv
Playgrounds updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/playgrounds/

044227.csv
Public toilets updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/public-toilets/

044214.csv
Dog parks updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/dog-parks/

044226.csv
Public art updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/public-art/

044209.csv
City cycle stations updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/citycycle-stations/

044237.csv
Wifi hotspots updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/wifi-hot-spots/

048001.csv - no equivalent in mdb
free plant nurseries
http://data.brisbane.qld.gov.au/index.php/dataset/free-plant-nurseries/

047783 - no equivalent in mdb
approved pet shops updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/approved-pet-shops-2/

047439.csv - no equivalent in mdb
helmet retail outlets updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/citycycle-helmet-retail-outlets/

044359.csv
Picnic areas updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/picnic-areas/

044206.csv
Bus stops updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/bus-stops/

044203.csv
Boat ramps updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/boat-ramps/

044215.csv
Ferry terminals updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/ferry-terminals/

044204.csv
Access ratings updated yearly
http://data.brisbane.qld.gov.au/index.php/dataset/brisbane-access-ratings-database/
