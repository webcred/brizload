﻿'Copyright (c) 2012 Steve Shearn. All rights reserved.

'Permission is hereby granted, free of charge, to any person obtaining a
'copy of this software and associated documentation files (the "Software"), 
'to deal in the Software without restriction, including without limitation 
'the rights to use, copy, modify, merge, publish, distribute, sublicense, 
'and/or sell copies of the Software, and to permit persons to whom the 
'Software is furnished to do so, subject to the following conditions:

'The above copyright notice and this permission notice shall be included in
'all copies or substantial portions of the Software.

'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
'IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
'FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
'AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
'FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
'DEALINGS IN THE SOFTWARE.

Public Class PublicArt

    Private append As Boolean = False

    Public Sub New(ByVal append As Boolean)
        Me.append = append
    End Sub

    Public Function CheckHeader(ByVal cols() As String) As Boolean
        If cols.Count <> 14 Then Return False

        If Not cols(0).Trim.Equals("Primary Maker name only", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(1).Trim.Equals("Name Title", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(2).Trim.Equals("Brief Description", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(3).Trim.Equals("Media Materials Description", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(4).Trim.Equals("Primary Prod Date", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(5).Trim.Equals("Measurement Reading primary system Y display only", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(6).Trim.Equals("Installation Information", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(7).Trim.Equals("Object Type", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(8).Trim.Equals("Public Art Subject", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(9).Trim.Equals("Public Art Theme", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(10).Trim.Equals("Street Location", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(11).Trim.Equals("Suburb", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(12).Trim.Equals("Latitude export", StringComparison.CurrentCultureIgnoreCase) Then Return False
        If Not cols(13).Trim.Equals("Longitude export", StringComparison.CurrentCultureIgnoreCase) Then Return False

        Return True
    End Function

    ' note the database fields don't match the csv dataset
    ' db fields are:
    ' 0  ID
    ' 1  Primary Maker name only
    ' 2  Primary Prod Date
    ' 3  Name Title
    ' 4  Brief Description
    ' 5  Media Materials Description
    ' 6  Measurement Reading primary system Y display only
    ' 7  Installation Information
    ' 8  Object Type
    ' 9  Public Art Subject
    ' 10 Public Art Theme
    ' 11 Street Location
    ' 12 Latitude
    ' 13 Longitude
    Public Sub Parse(ByVal Conn As Data.OleDb.OleDbConnection, ByVal outpath As String)
        Dim sql = "SELECT * FROM PUBLIC_ART"
        Using Cmd As New OleDb.OleDbCommand(sql, Conn)
            Dim rdr = Cmd.ExecuteReader()
            Using sw As New System.IO.StreamWriter(outpath, Me.append)
                While rdr.Read()
                    If String.IsNullOrEmpty(rdr(12).ToString) Then Continue While
                    If String.IsNullOrEmpty(rdr(13).ToString) Then Continue While

                    Dim sbName As New System.Text.StringBuilder()
                    sbName.Append(rdr(3))
                    Module1.CleanUp(sbName)

                    Dim sbAddress As New System.Text.StringBuilder()
                    sbAddress.Append(rdr(7))
                    'sbAddress.Append(" " & rdr(11).ToString)
                    Module1.CleanUp(sbAddress)

                    Dim sbInfo As New System.Text.StringBuilder()
                    If Not String.IsNullOrEmpty(rdr(1).ToString) Then
                        sbInfo.Append(" By: " & rdr(1).ToString)
                        If Not String.IsNullOrEmpty(rdr(2).ToString) Then sbInfo.Append(", " & rdr(2).ToString)
                    End If
                    sbInfo.Append(vbCrLf & "Description: " & rdr(4).ToString)
                    sbInfo.Append(vbCrLf & "Materials: " & rdr(5).ToString)
                    sbInfo.Append(vbCrLf & "Size: " & rdr(6).ToString)
                    sbInfo.Append(vbCrLf & "Subject: " & rdr(9).ToString)
                    sbInfo.Append(vbCrLf & "Theme: " & rdr(10).ToString)
                    Module1.CleanUp(sbInfo)
                    If sbInfo.Length > 2000 Then sbInfo = New System.Text.StringBuilder(sbInfo.ToString(0, 1999))

                    Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                    sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                    sb.Append(" VALUES (")
                    sb.Append("""publicart"",")
                    sb.Append("""" & rdr(12).ToString & """,")
                    sb.Append("""" & rdr(13).ToString & """,")
                    sb.Append("""" & sbName.ToString & """,")
                    sb.Append("""" & sbAddress.ToString & """,")
                    sb.Append("""" & sbInfo.ToString & """")
                    sb.Append(");")

                    sw.WriteLine(sb.ToString)
                End While

                sw.Close()
            End Using
        End Using
    End Sub

    Public Sub Parse(ByVal rdr As Microsoft.VisualBasic.FileIO.TextFieldParser, ByVal outpath As String)
        Using sw As New System.IO.StreamWriter(outpath, Me.append)
            While Not rdr.EndOfData
                Dim vals = rdr.ReadFields()

                If String.IsNullOrEmpty(vals(12)) Then Continue While
                If String.IsNullOrEmpty(vals(13)) Then Continue While

                Dim sbName As New System.Text.StringBuilder()
                sbName.Append(vals(1))
                Module1.CleanUp(sbName)

                Dim sbAddress As New System.Text.StringBuilder()
                sbAddress.Append(vals(6))
                sbAddress.Append(" " & vals(10))
                sbAddress.Append(", " & vals(11))
                Module1.CleanUp(sbAddress)

                Dim sbInfo As New System.Text.StringBuilder()
                If Not String.IsNullOrEmpty(vals(0)) Then
                    sbInfo.Append(" By:" & vals(0))
                    If Not String.IsNullOrEmpty(vals(4)) Then sbInfo.Append(", " & vals(4))
                End If
                sbInfo.Append(vbCrLf & "Description: " & vals(2))
                sbInfo.Append(vbCrLf & "Materials: " & vals(3))
                sbInfo.Append(vbCrLf & "Size: " & vals(5))
                sbInfo.Append(vbCrLf & "Subject: " & vals(8))
                sbInfo.Append(vbCrLf & "Theme: " & vals(9))
                Module1.CleanUp(sbInfo)

                Dim sb As New System.Text.StringBuilder("INSERT INTO `places` ")
                sb.Append("(`code`, `latitude`, `longitude`, `name`, `address`, `info`)")
                sb.Append(" VALUES (")
                sb.Append("""publicart"",")
                sb.Append("""" & vals(12) & """,")
                sb.Append("""" & vals(13) & """,")
                sb.Append("""" & sbName.ToString & """,")
                sb.Append("""" & sbAddress.ToString & """,")
                sb.Append("""" & sbInfo.ToString & """")
                sb.Append(");")

                sw.WriteLine(sb.ToString)
            End While

            sw.Close()
        End Using
    End Sub

End Class
